package org.onedigit

def onedigit() {
    println "This is the OneDigitPipeline"
    println 'Here we can do almost anything!'
    def x = Math.cos(Math.PI)
    println "Cos PI = $x"
}

return this

