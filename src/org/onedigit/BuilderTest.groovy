package org.onedigit

/**
 * See https://dzone.com/articles/groovy-resolve-strategies
 */

class SampleBuilder {
    private savedObjects = [:]

    void propertyMissing(String name, value) {
        savedObjects[name] = value
    }

    void printSavedObjects() {
        println 'Saved objects:'
        savedObjects.each {
            println "${it.key} = ${it.value}"
        }
    }

    static build(Closure c) {
        def builder = new SampleBuilder()
        c.delegate = builder
        c.resolveStrategy = Closure.DELEGATE_FIRST
        c()
        builder
    }
}

/*
class Application {
    static void main(String[] args) {
        def res = SampleBuilder.build {
            name1 = 'value1'
            name2 = 'value2'
        }
        
        res.printSavedObjects()
    }
}

*/

def res = SampleBuilder.build {
    name1 = 'value1'
    name2 = 'value2'
}
res.printSavedObjects()
